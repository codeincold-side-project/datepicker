# Date Picker

It's a side project for **practice**. Try to implement a date picker **without any date library** such like Day.js and Moment.js. You can change date by updating input value or selecting date directly from calendar.

![](https://i.imgur.com/Y0NZSpsm.png)

## Author

- [Codeincold](https://gitlab.com/codeincold)

## Demo

[Demo Link](https://jasonjiang.info/datepicker)

## Tech Stack

- Framework: React CRA
- Css processor: Tailwind CSS
- Icon: Font Awesome
- Redux: Redux, Redux Toolkit, Redux Saga
- Git: Husky and Commitlint

## API document

| Name | Type | default | description |
| :---: | :---: | :---: | :---: |
| defaultDate | Date | new Date() | default value for setup input and calendar  |
| onSelectDate | ({year, month, day})=> void |  | callback when select a date from calendar  |
| onInputChange | ({year, month, day})=> void |  | callback when input value change. It will trigger when valid date value filled in input |
| inputContainerClass | string | "" | input container className |
| calendarContainerClass | string | "" | calendar container className |
